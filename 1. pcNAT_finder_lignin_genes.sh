#!/bin/bash
# ppcNATFinder
# Homologous of 46 lignin-related genes of Zea mays (INPUT) in 71 organisms available in Ensembl Compara Database were used as target to find and quantify putative protein coding NATs (Natural Antisense Transcripts) based on cDNA public data.
# tools used in this script: Ensembl Compara database API + seqkit + ORFFinder 

#####################  LIGNIN-RELATED HOMOLOGS ###########################
#INPUT
#1.query_genes_46.list
#Compara problem: change Zm00001d052841 by Zm00001d052840 Zm-B73-REFERENCE-GRAMENE-4.0 
#1.query_genes_46_v2.list

# MIN homology percentage requirement was established based on Arabidopsis thaliana known set of lignin genes (14 ids). 
# A target and query identity of 50 was enough to find out them. 
for i in 50 60 70; do 
get-orthologs-compara  -v1 -i 1.query_genes_46_v2.list -org_list Zea_mays -type all -ident_target $i -ident_query $i > $i.tab
#the whole list in this database is 71 organisms. 
for i in $(ls *tab);do grep -v ';' $i | grep Arabidopsis_thaliana | cut -f1 | sort -V | uniq | wc -l;done
for i in $(ls *tab);do grep -v ';' $i | grep Arabidopsis_thaliana | cut -f1 | sort -V | uniq | grep -i -Fwf <(cut -f1 Ath_lignin_genes.list)| wc -l; done
# 3 # 70 70
# 9 # 60 60 
# 14 # 50 50 
#>>>> 50 50 with a total of 28 hits with Ath.
2.query_genes_46_v2_homologs_8310.csv
#Still some redundancy; as hits are ordered greater to lower identity, we take the first match per target id.
awk '!seen[$1]++' 2.query_genes_46_v2_homologs_8310.csv > 3.query_genes_46_v2_homologs_2688.csv

####################### HOMOLOGS TO cDNAs ID #############################
mkdir cdnas
cd cdnas
#save two columns of transcript ID and organism to be used later
awk '{split ($2,o,".");print $1,o[1]}' ../3.query_genes_46_v2_homologs_2688.csv > 4.id_organism.list
#extracts the ftp address
awk '{print $2"/cdna/"$2}' 4.id_organism.list | sed 's/^\(.\)/\L\1/' | sort -u > 5.organism_ftp.list
#download and decompress cDNA files
for i in $(cat 5.organism_ftp.list)
do 
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-46/fasta/${i}.*.cdna.all.fa.gz
done
for i in $(ls *gz); do gunzip ${i};done
cd ..
#I need to edit cdnas header in order to fetch the sequence with samtools faidx
for i in $(ls *cdna.all.fa); do sed 's/>.*gene:/>/g' ${i} > ${i}.fa; done 
#works!

################### cDNAs TO ORFs BIGGER THAN 50AA #######################
mkdir orf 
cd orfs
cat ../cdnas/3.query_genes_46_v2_homologs_1047_nondup.list | \
while read n k
do 
samtools faidx ${k}*cdna.all.fa.fa ${n} -o ${k}.${n}.cdna.fa
ORFfinder -in ${k}.${n}.cdna.fa -strand plus -ml 75 |\
seqkit sort -lr | seqkit head -n1 > ${k}.${n}.orfplus.fa
ORFfinder -in ${k}.${n}.cdna.fa -strand minus -ml 75 |\
seqkit sort -lr | seqkit head -n1 > ${k}.${n}.orfminus.fa
done
ls *orfplus.fa > tmp1
for i in $(ls *orfplus.fa); do grep -v "^>" ${i} | wc -c; done > tmp2
for i in $(ls *orfminus.fa); do grep -v "^>" ${i} | wc -c; done > tmp3
paste tmp1 tmp2 tmp3 |awk '{if (($3*100+0.00001)/($2+0.00001)>=50) A="YES"
else if (($3*100+0.00001)/($2+0.00001)<50) A="NO"
else A="NO_ORF"
print $1,$2,$3,($3*100+0.00001)/($2+0.00001),A}' | \
sed 's/.orfplus.fa//' > Results_Orthologs_compara.csv
