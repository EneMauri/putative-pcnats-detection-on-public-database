# cDNA sequences and gene annotation are available on the ensembl FTP site
# cDNA is the mature mRNA sequence, including UTR and leaving apart introns.
# How many genes have a putative NAT sequence per genome?

#http://plants.ensembl.org/info/data/ftp/index.html

cut -f1 dataset.csv | tr '[:upper:]' '[:lower:]' | sed 's/ /_/g' > organisms.list
FTP="ftp://ftp.ensemblgenomes.org/pub/plants/release-50/"

for organism in $(cat organisms.list)
do
wget $FTP"gff3/"$organism"/"*gff3.gz
wget $FTP"fasta/"$organism"/cdna/"*cdna.all.fa.gz
done

#Decompress
for i in $(ls *gz); do gunzip ${i}; done

#Edit cdnas files to later use samtools faidx
for i in $(ls *cdna.all.fa); do sed -i 's/>.*gene:/>/g' ${i}; done

#Index fasta
for i in $(ls *all.fa); do samtools faidx ${i}; done

#Reedit organims list 
sed -i 's/^\(.\)/\U\1/' organisms.list

#extract the list of genes
for organism in $(cat organisms.list)
do grep -v '#' ${organism}*.gff3 | awk '$3=="gene"{split($9,s,";");print s[1]}' | \
sed 's/ID=gene://g' > ${organism}_genes.list
done

