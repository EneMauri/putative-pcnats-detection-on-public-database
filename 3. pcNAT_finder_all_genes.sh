for organism in $(cat organisms.list | grep ^S)
do
cat ${organism}_genes.list | while read id;
# EXTRACTS CDNA, ITS ORF IN THE ANNOTATED STRAND AND ITS PCCNAT (PUTATIVE PROTEIN CODING) AS THE LONGEST ONE.
do
samtools faidx $organism*cdna.all.fa $id -o $organism.$id.cdna.fa
ORFfinder -in $organism.$id.cdna.fa -strand plus -ml 75 | seqkit sort -lr | seqkit head -n1 > $organism.$id.orfplus.fa
ORFfinder -in $organism.$id.cdna.fa -strand minus -ml 75 | seqkit sort -lr | seqkit head -n1 > $organism.$id.orfminus.fa
done

# EDITS CSV RESULTS TABLE
cat ${organism}_genes.list | while read id
do ls $organism.$id.orfplus.fa; done > $organism.tmp1
cat ${organism}_genes.list | while read id
do grep -v "^>" $organism.$id.orfplus.fa | wc -c; done > $organism.tmp2
cat ${organism}_genes.list | while read id
do grep -v "^>" $organism.$id.orfminus.fa | wc -c; done > $organism.tmp3
paste $organism.tmp1 $organism.tmp2 $organism.tmp3 | \
awk '{if (($3*100+0.00001)/($2+0.00001)>=50) A="YES"
else if (($3*100+0.00001)/($2+0.00001)<50) A="NO"
else A="NO_ORF"; print $1,$2,$3,($3*100+0.00001)/($2+0.00001),A}' | \
sed 's/.orfplus.fa//' > $organism.csv

# CREATING FASTA FILES FOR GENES AND THEIR PCCNATS
awk '$5=="YES"{print $1}' $organism.csv | while read gene; do cat ${gene}.orfminus.fa; done > ${organism}_yes_orfminus.fa
awk '$5=="YES"{print $1}' $organism.csv | while read gene; do cat ${gene}.orfplus.fa; done > ${organism}_yes_orfplus.fa
#awk '{print $1}' $organism.csv | while read gene; do cat ../112_organisms_cdnas/${gene}.cdna.fa; done > ${organism}_cdnas.fa

# REMOVING SECONDARY FILES
cat ${organism}_genes.list | while read id
do
rm $organism.$id.cdna.fa
rm $organism.$id.orfplus.fa
rm $organism.$id.orfminus.fa
done
done
