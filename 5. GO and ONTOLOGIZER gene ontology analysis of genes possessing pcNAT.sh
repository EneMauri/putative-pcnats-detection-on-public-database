# 1. Putative pcNAT ratio CALCULATION
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ ./rate.sh 
Brassica_rapa.csv	4.87805e-05
Chlamydomonas_reinhardtii.csv	0.536437
Chondrus_crispus.csv	0.294586
Chara_braunii.csv	0.242842
Chenopodium_quinoa.csv	0.228386
Cucumis_melo.csv	0.206721
Cucumis_sativus.csv	0.151808
Coffea_canephora.csv	0.145851
Cannabis_sativa_female.csv	0.136739
Camelina_sativa_part1.csv	0.132905
Citrus_clementina.csv	0.13056
Corchorus_capsularis.csv	0.129684
Cyanidioschyzon_merolae.csv	0.124599
Citrullus_lanatus.csv	0.120891
Capsicum_annuum.csv	0.0849491
Zea_mays.csv	0.0483696

#Zea mays only has a 5% of putative pcNATs
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ grep YES Zea_mays.csv | wc -l
1915
nauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ awk '$5=="YES"{print $1}' Zea_mays.csv | sed 's/Zea_mays.//g' | sort > Zea_mays_yes_ids.list
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ cat ../../*gff3 | awk '$3=="gene"' | grep -Fwf Zea_mays_yes_ids.list | wc -l
1915

# 2. POSITION OF ppcNATs
#position of putative pcNATs per chromosomes not enriched
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ cat ../../*gff3 | awk '$3=="gene"' | grep -Fwf Zea_mays_yes_ids.list |cut -f1 | sort -V | uniq -c
#position of ... along chromosomes, some are operon-like (several contiguous genes) 
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ cat ../../*gff3 | awk '$3=="gene"' | grep -Fwf Zea_mays_yes_ids.list |cut -f1,4,5 | sort -V > Zea_mays_yes_ids.distri

#3. GO ENRICHMENT ANALYSIS OF GENES WITH ppcNATs
http://geneontology.org/docs/go-enrichment-analysis/
http://ontologizer.de/commandline/
http://current.geneontology.org/
# -g GO terminology and structure
wget http://current.geneontology.org/ontology/go.obo
# -a (annotations)
#GAF, GPAD and GPI files are also available from the /annotations/ of the current release on http://current.geneontology.org
#To extend outcomes of the work described here for future v4 efforts, maize‐GAMER Aggregate annotations have also been created for the maize B73 RefGen_v4, 
#which can be accessed at MaizeGDB (http://download.maizegdb.org/maize-GAMER) and via CyVerse (doi.org/10.7946/P2M925).
https://datacommons.cyverse.org/browse/iplant/home/shared/commons_repo/curated/Carolyn_Lawrence-Dill_maize-GAMER_maize.B73_RefGen_v4_Zm00001d.2_Oct_2017.r1/e.agg_data
#https://pubmed.ncbi.nlm.nih.gov/31245718/
# -p (complete list of genes)
awk '$3=="gene"{split($9,r, ";"); print r[1]}' ../../Zea_mays.B73_RefGen_v4.50.gff3 | sed 's/ID=gene://g' > Zea_mays_all_ids.list
# -s (study list of genes)

#ONTOLOGIZER ANALYSIS
java -jar Ontologizer.jar -g go.obo -a maize.B73.AGPv4.aggregate.gaf -s Zea_mays_yes_ids.list -p Zea_mays_all_ids.list
awk '/^ID/ || $11<=0.05' table-Zea_mays_yes_ids-Parent-Child-Union-None.txt> GOenrichment.csv
#Negative regulation of metabolism, and survival functions are enriched, lignin metabolism is just one case.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
dot -T png view-Zea_mays_yes_ids-Parent-Child-Union-None.dot -o view-Zea_mays_yes_ids-Parent-Child-Union-None.png #go METABOLISM
sudo apt install graphviz
dot -T png view-Zea_mays_yes_ids-Parent-Child-Union-None.dot -o view-Zea_mays_yes_ids-Parent-Child-Union-None.png

# HOMOLOGY ANALYSIS OF pccNATs
awk '$5=="YES"{print $1}' Zea_mays.csv | while read id; do cat ../112_organisms_cdnas/${id}.orfminus.fa; done > Zea_mays_yes_orfminus.fa
awk '$5=="YES"{print $1}' Zea_mays.csv | while read id; do cat ../112_organisms_cdnas/${id}.orfplus.fa; done > Zea_mays_yes_orfplus.fa
awk '{print $1}' Zea_mays.csv | while read id; do cat ../112_organisms_cdnas/${id}.cdna.fa; done > Zea_mays_cdnas.fa
nmauri@nmauri-HP-EliteBook-840-G7-Notebook-PC:/media/nmauri/SHAULA/ZEA/ALBERT_MUÑOZ/pcNATfinder/ratio_per_organism$ grep -c '>' Zea_mays_yes*
Zea_mays_yes_orfminus.fa:1859
Zea_mays_yes_orfplus.fa:1860




